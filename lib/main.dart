import 'package:flutter/material.dart';
import 'package:template_palette_g_by_img/pages/team_member_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Template | Palette G',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: Colors.white,
          canvasColor: Colors.white,
          appBarTheme: AppBarTheme(
              elevation: 0,
              color: Colors.white
          )
      ),
      home: TeamMemberScreen(),
    );
  }
}
