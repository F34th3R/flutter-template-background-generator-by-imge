import 'package:flutter/material.dart';
import 'package:template_palette_g_by_img/utils/sizes.dart';
import 'package:template_palette_g_by_img/utils/text_styles.dart';
import 'package:template_palette_g_by_img/widgets/member_widget.dart';
import 'package:template_palette_g_by_img/model/member.dart';

class TeamMemberScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.keyboard_backspace),
        title: Text('Our Team', style: appBarTextStyle,),
      ),
      body: Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Container(),
          ),
          Expanded(
            flex: 10,
            child: ListView.separated(
              itemCount: members.length,
              separatorBuilder: (context, index) {
                return SizedBox(height: size_20,);
              },
              itemBuilder: (context, index) {
                return MemberWidget(
                  member: members[index],
                  compactMode: false
                );
              },
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(),
          )
        ],
      ),
    );
  }
}
