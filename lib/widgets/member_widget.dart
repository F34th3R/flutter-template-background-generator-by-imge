import 'package:flutter/material.dart';
import 'package:palette_generator/palette_generator.dart';
import 'package:template_palette_g_by_img/model/member.dart';
import 'package:template_palette_g_by_img/pages/member_details_screen.dart';
import 'package:template_palette_g_by_img/utils/sizes.dart';
import 'package:template_palette_g_by_img/utils/text_styles.dart';
import 'package:template_palette_g_by_img/widgets/clipped_image_widget.dart';
import 'package:template_palette_g_by_img/widgets/name_widget.dart';

class MemberWidget extends StatelessWidget {
  final Member member;
  final bool compactMode;

  const MemberWidget({Key key, this.member, this.compactMode = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _generatePalette(context, member.imagePath)
          .then((_palette) {
            Navigator.push(context, MaterialPageRoute(
              builder: (context) => MemberDetailsPage(member: member, palette: _palette)
            ));
          });
      },
      child: Row(
        children: <Widget>[
          ClippedImageWidget(imagePath: member.imagePath, compactMode: compactMode),
          SizedBox(width: size_20,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              NameWidget(name: member.name, style: nameStyle),
              SizedBox(height: size_8,),
              Text(member.occupation, style: occupationStyle,)
            ],
          )
        ],
      ),
    );
  }

  Future<PaletteGenerator> _generatePalette(context, String imagePath) async {
    PaletteGenerator _paletteGenerator = await PaletteGenerator.fromImageProvider(AssetImage(imagePath),
      size: Size(110, 250), maximumColorCount: 20);
    return _paletteGenerator;
  }
}
