import 'package:flutter/material.dart';
import 'package:template_palette_g_by_img/utils/sizes.dart';

class ClippedImageWidget extends StatelessWidget {

  final String imagePath;
  final bool compactMode;
  static const BorderRadius _cardBorderRadius = BorderRadius.all(Radius.circular(size_12));

  const ClippedImageWidget({Key key, this.imagePath, this.compactMode}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: compactMode ? size_0 : size_8,
      borderRadius: _cardBorderRadius,
      child: ClipRRect(
        borderRadius: _cardBorderRadius,
        child: Image.asset(
          imagePath,
          height: compactMode ? 100 : 150,
          width: compactMode ? 80 : 110,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
